# -*- coding: utf-8 -*-
#
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
# Copyright (C) 2017       Kübra Cıngıllı <kubracingilli@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

TODO make the application run if this file is run

(maybe copy main.py to here)

'''

import inspect

from src.examples import *
from src.hint_giver import *
from src.storage_management import *
from Tkinter import *
import tkMessageBox
from functools import partial
from src.gui import *

class Main:
    def __init__(self):
        #self.n = 0
        #self.questionno = 4
        self.exampleDict = [poster_example, constant_example1, constant_example2, variable_example1]

    def main(self):
        root = Tk()
        progress = ProgressStorage(self.exampleDict)
        ##print progress.exampleName
        appgui = Gui(root, progress)
        root.mainloop()

if __name__ == '__main__':
    app = Main()
    app.main()
