# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# this parser can be used as follows:

from sets import Set
from checker import *

# user_answer is taken from the user, specific for this example
# expected_answer is solution answer set for this example
def test_case_joke(users_as):
  # expected answer set for this example is defined locally
  expected_as = Set('hear_joke')

  # set holding all possible hints
  # define hints for three different categories separately?????
  # possible_hints=Set(['Check sytnax of your program once more','Think about adding some more rules to eliminate unwanted aswer sets','It  seems that right answer set is eliminated by some rule'])

  # call check_syntax method to check is there any syntax error in user's answer
  syntax_error = check_syntax(prog)
  # if syntax error exists
    # TODO: call hint giver to produce appropriate hint
  # else
    # pass

  # call get_extra_as method to get extra answer sets
  extra_as = get_extra_as(users_as, expected_as)
  # if extra_as list is not empty
    # TODO: call hint giver
  # else
    # pass

  # call get_missing_as method to get missing answer sets
  missing_as = get_missing_as(users_as, expected_as)
  # if missing_as list is not empty
    # TODO: call hint giver
  # else
    # pass

testcases = [
    (constant_example2,'user ınput wrong', 'expected hınt'),
    (poster_example,'uyckjvöhjv','UnicodeEncodeError: \'ascii\' codec can\'t encode character u\'\xf6\' in position 28: ordinal not in range(128)'),
    (poster_example,'reach(A,B):- edge(A,C).','missing rule'),
    (constant_example1,'joke. hear_joke :- joke.','missing atom')
]
