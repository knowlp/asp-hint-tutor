# -*- coding: utf-8 -*-

# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
import unittest
from src.gui import *

class TestGUi(unittest.TestCase):

    def setUp(self):
        pass

    def test_numbers_3_4(self):
        self.assertEqual( multiply(3,4), 12)

    def test_strings_a_3(self):
        self.assertEqual( multiply('a',3), 'aaa')

if __name__ == '__main__':
    unittest.main()
'''
#it must be checked uniqueness of the example ids.
