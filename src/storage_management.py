# -*- coding: utf-8 -*-

# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
# Copyright (C) 2017       Kübra Cıngıllı <kubracingilli@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import os
import inspect
from examples import *

#variables filename dict
#dict can be none or exist

class ProgressStorage:
    #exampleDict: All example object list. It will change with TaskGraph
    #studentID: student ID
    def __init__(self, exampleDict, studentID = None):

        self.exampleDict = exampleDict
        if studentID is None:
            self.studentID = 12345
        else:
            self.studentID = studentID

        #burda her duruma uygun olması için pathi ayarlamam lazım
        self.progressFile = "tests/" + str(self.studentID) + "_Progress.json"
        self.exampleName = []
        j = 0
        while j < len(self.exampleDict):
            exampleID = self.exampleDict[j].id
            self.exampleName.append(exampleID)
            j = j + 1

        #self.exampleName = exampleName
        ##print self.exampleName
        self.data = {}
        self.data['examples'] = []

        self.data = self.create_first_jsonfile_and_load_data()

    def add_new_example(self, name, state, lastgood, trueans):
        new = {
            'name': name,
            'state': state,
            'lastgood': lastgood,
            'trueans': trueans
        }

        self.data['examples'].append(new)
        with open(self.progressFile, 'w') as outfile:
            json.dump(self.data, outfile, indent=4)

        print "add_new_example"

    #l: to change lastgood answer of the user
    #s: to change the state of the answer (false or true)
    #t: to set the true answer of the user.
    def save_changes_in_jsonfile(self, examplename, usrinput, changingcode):
        print 'save_changes_in_jsonfile'
        i = 0
        for p in self.data['examples']:
            # to change smt inside dict
            if p['name'] == examplename:
                i = 1
                if changingcode == 'l':
                    p['lastgood'] = usrinput
                elif changingcode == 's':
                    p['state'] = usrinput
                elif changingcode == 't':
                    p['trueans'] = usrinput
                continue
                #newnew
        if i == 0:
            self.add_new_example(examplename, "false", usrinput, "")

        # to save the changes
        with open(self.progressFile, 'w') as outfile:
            json.dump(self.data, outfile, indent=4)

    def create_first_jsonfile_and_load_data(self):
        if not os.path.exists(self.progressFile):
            open(self.progressFile, "w+")
            self.add_new_example(self.exampleName[0], "false", self.exampleDict[0].givenPart, "")
        #Check whether the file is empty or not empty
        elif os.path.getsize(self.progressFile) == 0:
            self.add_new_example(self.exampleName[0], "false", self.exampleDict[0].givenPart, "")

        with open(self.progressFile) as outfile:
            return json.load(outfile)


if __name__ == '__main__':
    ex = [poster_example, constant_example1, constant_example2, variable_example1]
    progress = ProgressStorage(ex)
    del progress
