# -*- coding: utf-8 -*-

# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from examples import *
from data_structure import *
from hint_giver import *
import subprocess
import re
from checker import *



'''
Below by using these classes 2 aspects with 3 examples is presented
'''
#Gokhan object represents progress of Gokhan
gokhan = StudentProgress('150111030','Gokhan AVCI')
#Constants is an aspect object with id 1 and name FACT without any dependency
constants = Aspect('constants','Sabitler',set())
#add two examples to constants aspect
constants.add_example(constant_example1)
constants.add_example(constant_example2)
'''
Second aspect
'''
#Create Variables aspect with id 1 name VARIABLES and dependency 1
variables = Aspect('variables','Değişkenler',set([constants]))
#add created example to Variables aspect
variables.add_example(variable_example1)


std='''
road(berlin,potsdam).
road(x,y) :- road(y,x).
'''

ref='''
road(berlin, potsdam).
road(X,Y) :- road(Y,X).
'''

give_hint(std,ref)

inputs=[
'''
road(ber).
road(pot).
road(X,Y) :- road(Y,X).
''',
'''
road(ber).
road(pot).
road(x,Y) :- road(Y,x).
''',
'''
road(berlin).
road(potsdam).
road(x,Y) :- road (Y,x).
''',
'''
road(berlin).
road(potsdam).
road(x,Y) :- road (Y,x).
''',
'''
road(berlin, potsdam).
road(x,Y) :- road(Y,x).
''']
for i in inputs:
  print i
  print give_hint(i,ref)
