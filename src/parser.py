# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# this parser can be used as follows:

# import answersetparser
#
# structure = answersetparser.parse(text)
#
import ply.lex as lex
import ply.yacc as yacc
import sys
import re

class Literal:
  def __init__(self, pred, args=[]):
    self.pred = pred
    self.args = args
  def __str__(self):
    return '#Literal(${}$,terms({}))#'.format(self.pred, self.args)
  def __repr__(self):
    return self.__str__()

class NegLiteral:
  def __init__(self, pred):
    self.pred = pred
  def __str__(self):
    return 'NegLiteral({})'.format(self.pred)
  def __repr__(self):
    return self.__str__()

class Head:
  def __init__(self, pred, args=[]):
    self.pred = pred
    self.args = args
  def __str__(self):
    return 'Head({},body({}))'.format(self.pred, self.args)
  def __repr__(self):
    return self.__str__()

class Rule:
  def __init__(self, head=[], body=[]):
    pass

class Program:
  def __init__(self, rules=[]):
    pass

reserved = {
   'not' : 'NAF',
}

tokens = ['ID', 'VARIABLE',  'STRING', 'NUMBER', 'ANONYMOUS_VARIABLE',  'DOT', 'COMMA', 'QUERY_MARK', 'COLON', 'SEMICOLON', 'OR',  'CONS', 'WCONS', 'PLUS', 'MINUS', 'TIMES', 'DIV', 'AT', 'PAREN_OPEN', 'PAREN_CLOSE', 'SQUARE_OPEN', 'SQUARE_CLOSE', 'CURLY_OPEN', 'CURLY_CLOSE', 'EQUAL', 'UNEQUAL', 'LESS', 'GREATER', 'LESS_OR_EQ', 'GREATER_OR_EQ', 'AGGREGATE_COUNT', 'AGGREGATE_MAX', 'AGGREGATE_MIN', 'AGGREGATE_SUM', 'COMMENT'] + list(reserved.values())

# Here we specified regular expressions
# We excluded reserve words from ID of predicates
def t_ID(t):
  r'[a-zA-Z_][a-zA-Z_0-9]*'
  t.type = reserved.get(t.value,'ID') # Check for reserved words
  return t

# Regular expressions
t_VARIABLE = r'[A-Z_][A-Za-z0-9-_]*'
t_STRING = r'"[^"]*"'
t_NUMBER = r'[0-9]+'
t_ANONYMOUS_VARIABLE = r'_'
t_DOT = r'\.'
t_COMMA = r','
t_QUERY_MARK = r'\?'
t_COLON = r':'
t_SEMICOLON = r';'
t_OR = r'\|'
t_CONS = r':-'
t_WCONS = r':~'
t_PLUS = r'\+'
t_MINUS = r'\-'
t_TIMES = r'\*'
t_DIV = r'\/'
t_AT = r'@'
t_PAREN_OPEN = r'\('
t_PAREN_CLOSE = r'\)'
t_SQUARE_OPEN = r'\['
t_SQUARE_CLOSE = r'\]'
t_CURLY_OPEN = r'\{'
t_CURLY_CLOSE = r'\}'
t_EQUAL = r'='
t_UNEQUAL = r'!='
t_LESS = r'<'
t_GREATER = r'>'
t_LESS_OR_EQ = r'<='
t_GREATER_OR_EQ = r'>='
t_AGGREGATE_COUNT = "\#count"
t_AGGREGATE_MAX = r'\#max'
t_AGGREGATE_MIN = r'\#min'
t_AGGREGATE_SUM = r'\#sum'
t_ignore = ' \t\r\n'

def t_error(t):
  raise Exception("unexpected character '{}'\n".format(repr(t)))
  t.lexer.skip(1)

# Our grammar specification starts from here
# Program is set of statements, query not supported (related to database)
def p_root(p):
  '''root : statements query
            | statements
            | query'''
  if len(p) == 3:
    raise Exception("not implemented")
  elif len(p) == 2:
    p[0] = p[1]

# Statement or multiple statements
def p_statements(p):
  '''statements : statements statement
                  | statement '''
  # statements is a list of statement
  if len(p) == 3:
    #print "statements statement"
    p[0] = p[1] + [p[2]]
  # single statement
  elif len(p) == 2:
    #print "statement"
    p[0] = [p[1]]

def p_query(p):
  '''query : classical_literal QUERY_MARK'''
  raise Exception("not implemented")

# format of each statement
def p_statement(p):
  '''statement : CONS body DOT
                 | CONS DOT
                 | head CONS body DOT
                 | head CONS DOT
                 | head DOT
                 | WCONS body DOT SQUARE_OPEN weight_at_level SQUARE_CLOSE
                 | WCONS DOT SQUARE_OPEN weight_at_level SQUARE_CLOSE'''
  # statement: head DOT | statement: CONS DOT
  if len(p) == 3:
    if p[1] == ':-':
      # :- .
      #print "CONS DOT"
      pass
    else:
      # a(X).
      #print "head DOT"
      p[0] = Head([p[1]])
  # statement: CONS body DOT | statement: head CONS DOT
  elif len(p) == 4:
    if p[1] == ':-':
      # :- a(X).
      #print "CONS body DOT"
      p[0] = p[2]
    else:
      # a(X) :- .
      #print "head CONS DOT"
      p[0] = Head(p[1])
  # statement : head CONS body DOT
  elif len(p) == 5:
    # a(X) :- b(X).
    #print "head CONS body DOT"
    p[0] = Head(p[1],p[3])
  # statement : WCONS body DOT SQUARE_OPEN weight_at_level SQUARE_CLOSE
  else:
    raise Exception("weak constraints not implemented")

# head of the rule
def p_head(p):
  '''head : disjunction
            | choice '''
  # a v b. or a | b.
  #print "disjunction | choice"
  p[0] = p[1]

# body of the rule
def p_body(p):
  '''body : body COMMA naf_literal
            | naf_literal
            | NAF aggregate
            | aggregate  '''
  # body : naf_literal | body : aggregate
  if len(p) == 2:
    if p[1] == 'aggregate':
      #print "body : aggragates"
      p[0] = [p[1]]
    else:
      #print "body : naf_literal"
      p[0] = [p[1]]
  # body : NAF aggregate
  elif len(p) == 3:
    #print "NAF aggregate"
    p[0] = [p[2]]
  # body : body COMMA naf_literal
  else:
    #print "body COMMA naf_literal"
    p[0] = p[1] + [p[3]]

# if disjunctive facts
def p_disjunction(p):
  '''disjunction : disjunction OR classical_literal
                   | classical_literal
                   '''
  # disjunction : classical_literal
  if len(p) == 2:
    #print "classical_literal"
    p[0] = [p[1]]
  # disjunction : disjunction OR classical_literal
  else:
    #print "disjunction OR classical_literal"
    p[0] = p[1] + [p[3]]

# choice rules
def p_choice(p):
  '''choice : term binop CURLY_OPEN choice_elements CURLY_CLOSE binop term
              | CURLY_OPEN choice_elements CURLY_CLOSE binop term
              | CURLY_OPEN CURLY_CLOSE binop term
              | CURLY_OPEN choice_elements CURLY_CLOSE
              | term binop CURLY_OPEN CURLY_CLOSE binop term
              | term binop CURLY_OPEN choice_elements CURLY_CLOSE'''
  # choice : CURLY_OPEN choice_elements CURLY_CLOSE
  if len(p) == 4:
    #print "CURLY_OPEN choice_elements CURLY_CLOSE"
    p[0] = p[2]
  # choice : term binop CURLY_OPEN choice_elements CURLY_CLOSE | choice : CURLY_OPEN choice_elements CURLY_CLOSE binop term
  if len(p) == 6:
    if p[1] == 'term':
      # X>{choice_elements}
      #print "choice : term binop CURLY_OPEN choice_elements CURLY_CLOSE"
      p[0] = [p[1]] + [p[4]]
    else:
      #print "choice : CURLY_OPEN choice_elements CURLY_CLOSE binop term"
      # {choice_elements}<X
      p[0] = [p[2]] + [p[5]]
  # choice : term binop CURLY_OPEN choice_elements CURLY_CLOSE binop term
  if len(p) == 8:
    #print "choice : term binop CURLY_OPEN choice_elements CURLY_CLOSE binop term"
    # Y<{choice_elements}<X
    p[0] = p[4]

# choice elements in choice rule
def p_choice_elements(p):
  '''choice_elements :  choice_elements SEMICOLON
                        | choice_element'''
  #  choice_elements : choice_elements SEMICOLON
  if len(p) == 3:
    #print "choice_elements SEMICOLON"
    p[0] = p[1]
  # choice_element
  else:
    #print "choice_element"
    p[0] = [p[1]]

def p_choice_elemet(p):
  '''choice_element : classical_literal COLON naf_literals
                      | classical_literal COLON
                      | classical_literal'''
  # choice_element : classical_literal
  if len(p) == 2:
    #print "choice_element : classical_literal"
    p[0] = p[1]
  # choice_element : classical_literal COLON
  elif len(p) == 3:
    #print "choice_element : classical_literal COLON"
    p[0] = p[1]
  # choice_element : classical_literal COLON naf_literals
  else:
    #print "choice_element : classical_literal COLON naf_literals"
    # p[0] = [p[1]] + p[3]
    p[0] = p[3]

# aggregates
def p_aggregate(p):
  '''aggregate : term binop aggregate_function CURLY_OPEN aggregate_elements CURLY_CLOSE binop term
                 | aggregate_function CURLY_OPEN aggregate_elements CURLY_CLOSE binop term
                 | aggregate_function
                 | binop term
                 | aggregate_function CURLY_OPEN aggregate_elements CURLY_CLOSE
                 | term binop aggregate_function CURLY_OPEN CURLY_CLOSE binop term
                 | term binop aggregate_function CURLY_OPEN aggregate_elements CURLY_CLOSE'''
  raise Exception("aggregates not implemented yet")

def p_aggregate_elements(p):
  '''aggregate_elements : aggregate_elements SEMICOLON
                           | aggregate_element '''
  # aggregate_elements : aggregate_elements SEMICOLON
  if len(p) == 3:
    #print "aggregate_elememnts SEMICOLUMN "
    p[0] = p[1]
  # aggregate_elements : aggregate_element
  else:
    #print "aggregate_elemet"
    p[0] = [p[1]]

def p_aggregate_element(p):
  '''aggregate_element : basic_terms COLON naf_literals
                         | basic_terms COLON
                         | basic_terms
                         | COLON naf_literals'''
  # aggregate_element : basic_terms COLON naf_literals
  if len(p) == 4:
    #print "aggregate element : basic_terms COLON naf_literals"
    p[0] = p[1] + p[3]

  # aggregate_element : basic_terms COLON | aggregate_element : COLON naf_literals
  elif len(p) == 3:
    #print "aggregate_element"
    if p[1] == 'COLON':
      p[0] = p[2]
    else:
      p[0] = p[1]
  # aggregate_element : basic_terms
  else:
    #print "basic_terms"
    p[0] = p[1]

def p_aggregate_function(p):
  '''aggregate_function : AGGREGATE_COUNT
                          | AGGREGATE_MAX
                          | AGGREGATE_MIN
                          | AGGREGATE_SUM '''
  # aggregate_function
  #print "AGGREGATE"
  p[0] = str(p[1])

# weak constraint
def p_weight_at_level(p):
  '''weight_at_level : term AT term COMMA terms
                       | term AT term
                       | term COMMA terms'''
  raise Exception("weak constraints are not implemented")


def p_naf_literals(p):
  '''naf_literals : naf_literals COMMA naf_literal
                    | naf_literal '''
  # naf_literals : naf_literals COMMA naf_literal
  if len(p) == 4:
    #print "naf_literals COMMA naf_literal"
    p[0] = p[1] + [p[3]]

  # naf_literals : naf_literal
  else:
    #print "naf_literal"
    p[0] = [p[1]]

def p_naf_literal(p):
  '''naf_literal : NAF classical_literal
                   | classical_literal
                   | builtin_atom '''
  # naf_literal : NAF classical_literal
  if len(p) == 3:
    #print "NAF classical_literal"
    p[0] = NegLiteral(p[2])
  # naf_literal : builtin_atom
  else:
    #print "builtin_atom or classical_literal"
    p[0] = p[1]

def p_classical_literal(p):
   '''classical_literal : MINUS ID PAREN_OPEN terms PAREN_CLOSE
                          | ID PAREN_OPEN terms PAREN_CLOSE
                          | ID PAREN_OPEN PAREN_CLOSE
                          | ID
                          | MINUS ID PAREN_OPEN PAREN_CLOSE'''
   # classical_literal : ID
   if len(p) == 2:
     #print "classical_literal ID"
     p[0] = Literal(p[1])
     #p[0] = [p[1]]
   # classical_literal : ID PAREN_OPEN terms PAREN_CLOSE
   elif len(p) == 5 and p[1] != '-':
     #print "classical_literal ID PAREN_OPEN terms PAREN_CLOSE"
     #p[0] = "{}({})".format(p[1], ','.join(p[3]))
     p[0] = Literal(p[1], p[3])
     #p[0] = [p[1]] + [p[3]]

def p_builtin_atom(p):
  '''builtin_atom : term binop term'''
  # builtin_atom : term
  #if len(p) == 2:
  #  print "builtin_atom term"
  #  p[0] = p[1]
  ## builtin_atom : term binop term
  #else:
  #print "builtin_atom term binop term"
  p[0] = p[1] + p[2] + p[3]

def p_binop(p):
  '''binop : EQUAL
             | UNEQUAL
             | LESS
             | GREATER
             | LESS_OR_EQ
             | GREATER_OR_EQ'''
  p[0] = str(p[1])


def p_terms(p):
  '''terms : terms COMMA term
             | term'''
  # terms : terms COMMA term
  if len(p) == 4:
    #print "terms COMMA term"
    p[0] = p[1] + [p[3]]

  # terms : term
  else:
    #print "term : term"
    p[0] = [p[1]]


def p_term(p):
  '''term : ID PAREN_OPEN terms PAREN_CLOSE
            | ID
            | PAREN_OPEN PAREN_CLOSE
            | NUMBER
            | STRING
            | VARIABLE
            | ANONYMOUS_VARIABLE
            | PAREN_OPEN term PAREN_CLOSE
            | term arithop term'''
  # term : ID PAREN_OPEN terms PAREN_CLOSE
  if len(p) == 5:
    #print "term : ID PAREN_OPEN terms PAREN_CLOSE"
    #p[0] = "{}{}".format(p[1], p[3]) ????
    p[0] = [p[1]] + [p[3]]
  # term : ID | term : NUMBER ...
  elif len(p) == 2:
    #print "term : ID | NUMBER | STRING | VARIABLE"
    p[0] = str(p[1])
  # term : PAREN_OPEN term PAREN_CLOSE | term : term arithop term
  elif len(p) == 4:
    if p[1] == 'PAREN_OPEN':
      #print "PAREN_OPEN term PAREN_CLOSE"
      p[0] = [p[2]]
    else:
      #print " term arithop term"
      p[0] = p[1] + p[2] + p[3]

def p_basic_terms(p):
  '''basic_terms : basic_terms COMMA basic_term
                   | basic_term '''
  # basic_terms : basic_term
  if len(p) == 2:
    #print " basic_terms basic_term"
    p[0] = [p[1]]
  # basic_terms : basic_terms COMMA basic_term
  else:
    #print "basic_terms basic_terms COMMA basic_term"
    p[0] = p[1] + [p[3]]

def p_basic_term(p):
  '''basic_term : ground_term
                  | variable_term '''
  #print "variable_term | ground_term"
  p[0] = [p[1]]

def p_ground_term(p):
  '''ground_term : STRING
                   | MINUS NUMBER
                   | NUMBER'''
  if len(p) == 2:
    #print "ground_term STRING | ground_term NUMBER"
    p[0] = str(p[1])
  else:
    raise Exception("MINUS NUMBER ground terms not implemented yet")

def p_variable_term(p):
  ''' variable_term : VARIABLE
                      | ANONYMOUS_VARIABLE'''
  #print "VARIABLE | ANONYMOUS_VARIABLE"
  p[0] = str(p[1])

def p_arithop(p):
  ''' arithop : PLUS
               | MINUS
               | TIMES
               | DIV '''
  p[0] = str(p[1])

def p_error(p):
  raise Exception("unexpected Token '{}'\n".format(repr(p)))

def parse(content):
  # source file -> lexical analyzer (regular expressions) -> set of tokens -> parser (sytactical analysis) -> parse tree -> semantic analysis
  # create lexer which will take characters and produce tokens stream
  mylexer = lex.lex()
  # create parser starting at root or at PROGRAM
  myparser = yacc.yacc(start='root')
  # parse the all content
  return myparser.parse(content, lexer=mylexer)
