# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gokhan Avci <gokhannavci@gmail.com>
# Copyright (C) 2017       Kübra Cıngıllı <kubracingilli@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import inspect

from examples import *
from hint_giver import *
from Tkinter import *
import tkMessageBox
from functools import partial
from storage_management import *

class Gui(Frame):
    '''
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        '''
    def __init__(self, parent, progress):
        Frame.__init__(self, parent)
        self.root = parent
        self.n = 0
        self.progress = progress
        self.ans = self.progress.exampleDict[self.n].expectedAnswer

        self.text = Text(self.root)
        self.text.insert(INSERT, self.progress.exampleDict[self.n].givenPart)
        self.ltext = StringVar()
        self.ltext.set(self.progress.exampleDict[self.n].exampleText)
        self.label = Label(self.root, textvariable=self.ltext)
        # Menu Bar
        self.menu = Menu(self.root)
        self.root.config(menu=self.menu)
        # Main menu --> Examples
        self.filemenu = Menu(self.menu)
        self.menu.add_cascade(label="Examples", menu=self.filemenu)
        # Main menu --> Help
        helpmenu = Menu(self.menu)
        self.menu.add_cascade(label="Help", menu=helpmenu)
        helpmenu.add_command(label="About...", command=self.About)

        self.create_menubar_JSON()

        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.root.quit)
        self.b = Button(self.root, text="Submit Answer", command=self.clicked)
        self.hint = StringVar()
        self.label.pack()
        self.text.pack()
        self.b.pack(side=BOTTOM)
        Label(self.root, textvariable=self.hint).pack(side=TOP)

        #self.root.mainloop()

    # save_changes_in_jsonfile changingcode (last parameter)
    # l: to change lastgood answer of the user
    # s: to change the state of the answer (false or true)
    # t: to set the true answer of the user.
    def clicked(self):
        self.hint.set(give_hint(self.text.get(1.0, END), self.ans))
        self.progress.save_changes_in_jsonfile(self.progress.exampleName[self.n], self.text.get(1.0, END), 'l')
        self.filemenu.entryconfig(self.n + 1, command=partial(self.clicked_change_example, self.text.get(1.0, END), self.progress.exampleDict[self.n], self.n))
        if (self.hint.get() == "True ANSWER!"):
            self.progress.save_changes_in_jsonfile(self.progress.exampleName[self.n], 'true', 's')
            self.progress.save_changes_in_jsonfile(self.progress.exampleName[self.n], self.text.get(1.0, END), 't')
            self.filemenu.entryconfig(self.n+1, foreground='green')
            ##is the len true? check it.
            if (self.n == len(self.progress.exampleDict) - 1):
                self.ltext.set("You completed all questions successfully !!! ")
                self.text.delete(1.0, END)
                self.text.insert(INSERT, "CONGRATULATIONS!!!")
                self.b.configure(state=DISABLED)
            else:
                self.n = self.n + 1
                self.ans = self.progress.exampleDict[self.n].expectedAnswer
                self.ltext.set(self.progress.exampleDict[self.n].exampleText)
                self.text.delete(1.0, END)
                i = 0
                for p in self.progress.data['examples']:
                    if p['name'] == self.progress.exampleName[self.n]:
                        i = 1
                        self.text.insert(INSERT,p['lastgood'])
                if i == 0:
                    self.text.insert(INSERT, self.progress.exampleDict[self.n].givenPart)
                    self.progress.save_changes_in_jsonfile(self.progress.exampleName[self.n],  self.text.get(1.0, END), 's')
                    self.filemenu.entryconfig(self.n+1, foreground='white')
                    self.filemenu.entryconfig(self.n+1, command=partial(self.clicked_change_example, self.text.get(1.0, END), self.progress.exampleDict[self.n], self.n))

        else:
            self.progress.save_changes_in_jsonfile(self.progress.exampleName[self.n], 'false', 's')
            self.filemenu.entryconfig(self.n+1, foreground='white')

    # This function changes the current information with clicked one.
    def clicked_change_example(self, uanswer, obj, th):
        self.n = th
        self.ans = obj.expectedAnswer
        self.ltext.set(obj.exampleText)
        self.text.delete(1.0, END)
        self.text.insert(INSERT, uanswer)

    def create_menubar_JSON(self):
        # to create processed examples dropdown list
        i = 0
        with open(self.progress.progressFile) as json_file:
            self.progress.data = json.load(json_file)
            for p in self.progress.data['examples']:
                i = i + 1
                j = 0
                while j < len(self.progress.exampleDict):
                    if p['name'] == self.progress.exampleName[j]:
                        obj = self.progress.exampleDict[j]
                        th = j
                    j = j + 1
                try:
                    self.filemenu.add_command(label=p['name'], command=partial(self.clicked_change_example, p['lastgood'], obj, th))
                except:
                    print tkMessageBox.showinfo("!!!ERROR!!!", "Your Progress File Crashed.\nPlease Delete Your Progress File and Try Again.\n\nPs: You Will Loose All Your Progress.")
                    sys.exit(0)

                if p['state'] == "true":
                    self.filemenu.entryconfig(i, foreground='green')
                elif p['state'] == "false":
                    self.filemenu.entryconfig(i, foreground='white')
        # to create not processed examples dropdown list
        while i < len(self.progress.exampleDict):
            i = i + 1
            self.filemenu.add_command(label = self.progress.exampleName[i - 1])
            self.filemenu.entryconfig(i, foreground ='gray')

    def About(self):
        # info about the tool
        print tkMessageBox.showinfo("About", "Information about the tool")


'''
if __name__ == "__main__":
    n = 0
    questionno = 4
    jsonfilename = "tests/data.json"
    ex = [poster_example, constant_example1, constant_example2, variable_example1]
    exname = []
    j = 0
    while j < questionno:
        exname.extend(retrieve_name(ex[j]))
        j = j + 1
    print exname
    print ex
    root = Tk()
    app = Gui(root, n, questionno, jsonfilename, ex, exname)
    root.mainloop()

'''
