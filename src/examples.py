# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gökhan Avcı <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from data_structure import *

#Example for Variables aspect
variable_example1=Example('variable_example1','There are roads connecting cities.These roads are two way roads.And sometimes these roads can be blocked.'+'\n'+'In that situation we assume that both sides of the road is blocked.A route can be drawn from X to Y,if  there is a road from X to Y and this road is not blocked.'+'\n'+'We accept that we are in Berlin now.And if we can draw a route from our location to any X city,we can drive for X.'+'\n'+'We assumed that these roads are two way roads meaning that is there is road between X and Y,there is road between Y and X.'+'\n'+'Write a rule to realize this assumption.',
'road(berlin, potsdam).'+'\n'+'road(potsdam, werder).'+'\n'+'road(werder, brandenburg).','''
road(berlin, potsdam).
road(potsdam, werder).
road(werder, brandenburg).
road(X,Y) :- road(Y,X).
''')

#Two examples for Constant aspect
constant_example1 = Example('constant_example1','Assume that there is a joke.If there is joke and person is able to hear, then the person hears the joke.'+'\n'+'To understand the joke, person needs to hear the joke and not to be stupid. Lastly, person laughs when understands joke.'+'\n'+'Define hear_joke predicate satisfying that: If there is joke and person is able to hear, person hears the joke.'
,'joke.', '''
joke.
hear_joke :- joke, not deaf.
''')

#second example for constant aspect is creted
constant_example2 = Example('constant_example2','Peter,Adam,Tom,Anne,Bill,Beth,Cathy,Mary is our population.'+'\n'+'Adam’s father is Peter.Anne’s father is Tom.Bill’s father is Adam.Beth’s father is Adam.'+'\n'+'Adam’s mother is Cathy.Anne’s mother is Mary.Bill’s mother is Anne.Beth’s mother is Anne.'+'\n'+'If X is mother or father of Y,then X is parent of Y.Ancestor relationship also should be considered.'+'\n'+'Write facts using father and mother predicates realizing facts in our assumptions.',
'father(peter,adam).'+'\n'+'mother(cathy, adam).',
'''
father(peter,adam).
mother(cathy, adam).
father(tom,anne).
father(adam,bill).
father(adam,beth).
mother(mary, anne).
mother(anne, bill).
mother(anne, beth).
''')

poster_example= Example('poster_example','Assume that there are nodes and between nodes there are edges'+'\n'+
'These nodes are connected via edges'+'\n'+
'Write a rule for satisfying If there is edge between two node we can reach from one to another.'
+'\n'+'Write second rule for satisfying if we can reach from a to b (or vice versa)  and from b to c (or vice versa) ,'+'\n'+
'we can also reach from a to c (or vice versa) ','edge(x,y).'+'\n'+'edge(y,z).',
'''
edge(x,y).
edge(y,z).
reach(A,B) :- edge(A,B).
reach(A,C) :- reach(A,B),reach(B,C).
'''
)
