# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gokhan Avci <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
CHECKER TELLS :
1) SYNTACTICAL MISTAKES
2) EXTRA ANSWER SETS
3) MISSING ANSWER SETS

BY USING 3 GENERAL METHODS WHICH ARE:
check_syntax(userprogram)
get_extra_as(user,expected)
get_missing_as(user,expected)
'''
import subprocess
import json
import os
from parser import *

def get_atoms(prog):
  std_prog_parsed = parse(prog)
  A = []
  for item in std_prog_parsed:
    # Find between method will extract string between #, (#'s are added in parser to understand   where are literals)
    pos_atom = find_between(str(item), "#", "#" )
    pos_atom = re.sub('[!$]', '', pos_atom)
    A.append(pos_atom)
  return set(A)

def get_predicates(prog):
  std_prog_parsed = parse(prog)
  # preds will hold all predicates in the program
  # predicate names are surrounded by $ signs
  preds = []
  for item in std_prog_parsed:
    # predicate names will be between $ signs (again $ are added in the parser)
    predicate = find_between(str(item), "$", "$" )
    preds.append(predicate)
  preds = set(preds)
  return preds

def get_predarities(prog):
  std_prog_parsed = parse(prog)
  # predarities will hold predicates and their arity
  predarities = []
  for item in std_prog_parsed:
    pos_atom = find_between(str(item), "#", "#" )
    name = find_between(pos_atom, "$", "$")
    # count number of ","
    # we know that terms are between [], we will count number of commas there
    arity = find_between(pos_atom, "[", "]").count(',')
    terms = find_between(pos_atom, "[", "]")
    # if there are terms increment arity by 1.
    # , -> arity=2
    if terms != '':
      arity = arity+1
    predarities.append(name+"/"+str(arity))
  # remove duplicates
  predarities=set(predarities)
  return predarities

def get_constants(prog):
  A = get_atoms(prog)
  # const and var is a string holding constants and variables together
  const_and_vars=''
  for item in A:
    pos_atom = re.sub('[!$]', '', item)
    # get terms and remove ,
    string="{}".format(re.sub('[!,]','',find_between(pos_atom, "[", "]")))
    # make string of constants and variables
    const_and_vars=const_and_vars+" "+string+" "

  const_and_var=[]
  # remove ' signs
  newstr = const_and_vars.replace("'", "")
  # split constants and variables from string into list
  const_and_var = newstr.split()

  # constants will hold constants from the program
  constants = []
  # variables will hold variables from the program
  variables = []
  for item in const_and_var:
    # variable are starting with upper case
    if str(item).isupper():
      if str(item) != '':
        variables.append(item)
    else:
      if str(item) != '':
        constants.append(item)
  constants = set(constants)
  return constants

def get_variables(prog):
  A = get_atoms(prog)
  # const and var is a string holding constants and variables together
  const_and_vars=''
  for item in A:
    pos_atom = re.sub('[!$]', '', item)
    # get terms and remove ,
    string="{}".format(re.sub('[!,]','',find_between(pos_atom, "[", "]")))
    # make string of constants and variables
    const_and_vars=const_and_vars+" "+string+" "

  const_and_var=[]
  # remove ' signs
  newstr = const_and_vars.replace("'", "")
  # split constants and variables from string into list
  const_and_var = newstr.split()

  # constants will hold constants from the program
  constants = []
  # variables will hold variables from the program
  variables = []
  for item in const_and_var:
    # variable are starting with upper case
    if str(item).isupper():
      if str(item) != '':
        variables.append(item)
    else:
      if str(item) != '':
        constants.append(item)
  constants = set(constants)
  variables = set(variables)
  return variables

def get_constants_and_variables(prog):
  A = get_atoms(prog)
  # const and var is a string holding constants and variables together
  const_and_vars=''
  for item in A:
    pos_atom = re.sub('[!$]', '', item)
    # get terms and remove ,
    string="{}".format(re.sub('[!,]','',find_between(pos_atom, "[", "]")))
    # make string of constants and variables
    const_and_vars=const_and_vars+" "+string+" "

  const_and_var=[]
  # remove ' signs
  newstr = const_and_vars.replace("'", "")
  # split constants and variables from string into list
  const_and_var = newstr.split()

  # constants will hold constants from the program
  constants = []
  # variables will hold variables from the program
  variables = []
  for item in const_and_var:
    # variable are starting with upper case
    if str(item).isupper():
      if str(item) != '':
        variables.append(item)
    else:
      if str(item) != '':
        constants.append(item)
  constants = set(constants)
  variables = set(variables)
  return constants, variables

def check_syntax(usr):
  '''
  IF USER INPUT HAVE SYNTACTICAL ERRORS IT WILL RETURN 1
  ELSE IT RETURNS 0
  '''
  p = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE, stderr = subprocess.PIPE,stdout=subprocess.PIPE,shell=True)
  p_exp = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE,stderr = subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
  out, err = p.communicate(usr)
  data = json.loads(out)
  '''
  CHECK SYNTACTICAL ERROR
  '''
  p.stdin.close()
  p.stdout.close()
  check = data['Result']
  if check == "UNKNOWN":
    return 1,err
  return 0,err

def get_extra_as(usr,exp):
  '''
  THIS METHOD GIVES SET OF FROZENSETS WHICH ARE IN USER ANSWERSET
  AND ARE NOT IN EXPECTED ANSWERSET
  '''
  p = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE, stderr = subprocess.PIPE,stdout=subprocess.PIPE,shell=True)
  p_exp = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE,stderr = subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
  answerset = set()
  answerset_exp = set()
  out, err = p.communicate(usr)
  out2, err2 = p_exp.communicate(exp)
  data_exp = json.loads(out2)
  data = json.loads(out)
  p.stdin.close()
  p.stdout.close()
  p_exp.stdin.close()
  p_exp.stdout.close()
  for answersetinfo in data['Call'][0]['Witnesses']:
    answerset.add(frozenset(answersetinfo['Value']))

  for answersetinfo_exp in data_exp['Call'][0]['Witnesses']:
    answerset_exp.add(frozenset(answersetinfo_exp['Value']))
  return answerset - answerset_exp

def get_missing_as(usr,exp):
  '''
  THIS METHOD GIVES SET OF FROZENSETS WHICH ARE NOT IN USER ANSWERSET
  AND ARE IN EXPECTED ANSWERSET
  '''
  p = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE, stderr = subprocess.PIPE,stdout=subprocess.PIPE,shell=True)
  p_exp = subprocess.Popen("clingo --outf=2 0", stdin=subprocess.PIPE,stderr = subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
  answerset = set()
  answerset_exp = set()
  out, err = p.communicate(usr)
  out2, err2 = p_exp.communicate(exp)
  data_exp = json.loads(out2)
  data = json.loads(out)
  p.stdin.close()
  p.stdout.close()
  p_exp.stdin.close()
  p_exp.stdout.close()
  for answersetinfo in data['Call'][0]['Witnesses']:
    answerset.add(frozenset(answersetinfo['Value']))

  for answersetinfo_exp in data_exp['Call'][0]['Witnesses']:
    answerset_exp.add(frozenset(answersetinfo_exp['Value']))

  return answerset_exp - answerset

# This method finds string surrounded by 'first' and 'last' characters
def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""
