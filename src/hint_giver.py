# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gokhan Avci <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from checker import *

def give_hint(usr,exp):
  true_answer=0
  #while(true_answer == 0):
  hintisgiven=0
    #usr = get_usr_input()
  syntax_error,err = check_syntax(usr)
  #first check syntax
  if syntax_error == 1 :
    return "You have a syntax error"
    true_answer=0
    hintisgiven=1
  #check answer is true or not
  elif(len(get_missing_as(usr,exp))==0 and len(get_extra_as(usr,exp))==0):
    true_answer=1
  else:
    #check predicates
    extra_predicates = set()
    extra_predicates = get_predicates(usr) - get_predicates(exp)
    if(len(extra_predicates) != 0):
      return "You introduced predicates listed below:"+repr(extra_predicates)
      true_answer=0
      hintisgiven=1

    missing_predicates = set()
    missing_predicates = get_predicates(exp) - get_predicates(usr)
    if(len(missing_predicates) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou forgot to use predicates listed below:"+repr(missing_predicates)
      true_answer=0
      hintisgiven=1
    # check constants / variables
    extra_constants = set()
    extra_constants = get_constants(usr) - get_constants(exp)
    missing_constants = set()
    missing_constants = get_constants(exp) - get_constants(usr)
    extra_variables = set()
    extra_variables = get_variables(usr) - get_variables(exp)
    missing_variables = set()
    missing_variables = get_variables(exp) - get_variables(usr)
    # if an constant is used instead of a variable
    for varelement in extra_variables:
      if(varelement.lower() in missing_constants and hintisgiven==0):
        return "***New Hint ***\nYou used "+varelement+" as variable however it should be a constant"
        true_answer=0
        hintisgiven=1

    # if a variable is used instead of a constant
    for constelement in extra_constants:
      if(constelement.upper() in missing_variables and hintisgiven==0):
        return "***New Hint ***\nYou used "+constelement+" as constant however it should be a variable"
        true_answer=0
        hintisgiven=1


    # unexpected variables
    if(len(extra_variables) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou introduced variables listed below:"+repr(extra_variables)
      true_answer=0
      hintisgiven=1

    # missing variables
    if(len(missing_variables) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou forgot to use variables listed below:"+repr(missing_variables)
      true_answer=0
      hintisgiven=1
    #unexpected constants
    extra_constants = set()
    extra_constants = get_constants(usr) - get_constants(exp)
    if(len(extra_constants) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou introduced constants listed below:"+repr(extra_constants)
      true_answer=0
      hintisgiven=1
    #missing constants
    missing_constants = set()
    missing_constants = get_constants(exp) - get_constants(usr)
    if(len(missing_constants) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou forgot to use constants listed below:"+repr(missing_constants)
      true_answer=0
      hintisgiven=1

    # wrong arity
    usrarities=get_predarities(usr)
    exparities=get_predarities(exp)

    for predelement in usrarities:
      el=""
      pred=""
      el, pred = predelement.split('/')
      for element in exparities:
        el2, pred2=element.split('/')
        if(el == el2 and pred != pred2 and hintisgiven==0):
          return "***New Hint ***\nYou used "+el+" with wrong arity ("+pred+" instead of "+pred2+" arity)\n"
          true_answer=0
          hintisgiven=1

    extra = set()
    extra = get_extra_as(usr,exp)
    #extra answer sets
    if(len(extra) != 0 and hintisgiven==0):
      return "***New Hint ***\nYou have extra answer sets in your solution.We listed for you below :"+repr(extra)
      true_answer=0
      hintisgiven=1
    #missing answer sets
    missing = set()
    missing = get_missing_as(usr,exp)
    if(len(missing) != 0 and hintisgiven==0 ):
      return "***New Hint ***\nYou missed some answer sets in your solution.We listed for you below :"+repr(missing)
      true_answer=0
      hintisgiven=1
  # if AS are same
  if(true_answer ==1):
    return "True ANSWER!"
