# -*- coding: utf-8 -*-
# Tutoring System for Answer Set Programming with Smart Hints
# Copyright (C) 2016-2017  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2015-2016  Mustafa Mehuljic <mehuljic.mustafa@gmail.com>
# Copyright (C) 2015-2016  Gokhan Avci <gokhannavci@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Example:
    '''
    Example id is unique identifier for examples.

    There is exampleText where story is told and question is asked.

    And each example gives part of the solution to user.

    expectedAnswer field holds true answer for the example
    user's answer and true answer will be used by hint giver

    hints list will hold possible hints for the question
    this list will be filled by hintGiver NO
    this list will be given to the hintGiver and assist the
    hintGiver for giving hints that are specific to this example.

    The hintGiver should always work without data in self.
    Then it can give only very general hints, for example it does not
    know that blocked(X,Y) is about blocked _roads_ between cities X and Y.
    (Actually we might need more general information than hints,
    but this is a complex topic, let's leave it for later.)
    '''

    def __init__(self,i,exTxt,givenPart,expectedAnswer):
        self.id=i
        self.exampleText=exTxt
        self.givenPart=givenPart
        self.expectedAnswer=expectedAnswer
        self.hints=[]

    def add_hint(self,hint):
        self.hints.append(hint)


class Aspect:

    '''
    Each aspect has unique identifier,name and dependencies.
    Dependencies are a set of aspect IDs.As student
    proceed aspects get harder and will be dependent
    on previous aspects completion. So instead of saying
    4th aspect depends on 1,2,3 we prefer to say depends
    on 3. Because if user proceeds to 3 s/he must completed
    1 and 2.There is no need to waste space.
    Each aspect can have 1+ examples. So there is example
    list for each aspect.
    add_example method is used for adding examples to aspects.
    '''

    def __init__(self,i,name,dependency=set()):
        self.id=i
        self.name=name
        self.dependency=dependency
        self.examples = []

    def add_example(self,example):
        self.examples.append(example)

    def __str__(self):
        return "foo"

class StudentProgress:
    '''
    Student progress class holds id and name of the student

    Also completed aspects array is hold in this class
    complete_aspect function is used to extend completedAspects list when an aspect is completedAspects
    '''
    def __init__(self,i,name):
        self.id=i
        self.name=name
        self.completedExamples=set()

    def complete_example(self,example):
        self.completedExamples.add(example)

    def runExamples():
        pass

    if __name__ == '__main__':
        runExamples()
