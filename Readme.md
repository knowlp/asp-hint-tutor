[![codebeat badge](https://codebeat.co/badges/9692b5ba-c553-446f-9b68-ca50a350429c)](https://codebeat.co/projects/bitbucket-org-knowlp-asp-hint-tutor-master)

# Tutoring System for Answer Set Programming with Smart Hints  #

This repository contains a tutoring system for learning Answer Set Programming
in an interactive process where the system provides hints how to correct
the solution given by the student in case the solution is wrong.
The distinguishing feature of the system is, that the solution is not
revealed by these hints.

# Installation #

* Download Clingo version TODO

  https://github.com/potassco/clingo/releases

  Precompiled versions are fine, it is **not** necessary to have a Python-aware version.

  Clingo should be reachable in the PATH of the system as `clingo`.

* Install the python-tkinter interface

  TODO python version?

* Run the tutoring system

  From the root directory, run
  `python ./asp-hint-tutor.py`

  The result should be an interactive GUI.

# Configuration #

The following configuration options can be changed by editing `TODO.py`.

* The path/name for Clingo executable is set in variable `CLINGO_BIN`.
* TODO more?

# Citation #

* SIU paper TODO link and citation
* technical report TODO link and citation arXiv paper

# Maintainers #

* Peter Schüller <schueller.p@gmail.com>
* Marmara University, Istanbul
* KnowLP Research Group http://www.knowlp.com/

# Contributors #

* Peter Schüller <schueller.p@gmail.com> (2016-2017)
* Mustafa Mehuljic <mehuljic.mustafa@gmail.com> (2015-2016)
* Gökhan Avcı <gokhannavci@gmail.com> (2015-2016)